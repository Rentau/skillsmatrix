﻿using System.Collections.Generic;

namespace SkillsMatrix.UI.Web.Models
{
    public class TechViewModel
    {
        public List<DevTech> DevTechs { get; set; }
    }

    public class DevTech
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
