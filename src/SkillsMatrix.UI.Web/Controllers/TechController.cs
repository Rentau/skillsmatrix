﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SkillsMatrix.UI.Web.Models;

namespace SkillsMatrix.UI.Web.Controllers
{
    public class TechController : Controller
    {
        private readonly ITechService techService;

        public TechController(ITechService techService)
        {
            this.techService = techService;
        }

        // GET: Tech
        public ActionResult Index()
        {
            var technologies = this.techService.GetAll();


            var model = new TechViewModel
            {   
                DevTechs = new List<DevTech>
                {
                    new DevTech { Name = "C++", Description = "C++ Language"},
                    new DevTech { Name = "C#", Description = "C# Language"},
                    new DevTech { Name = "JS", Description = "JS is sucks"},
                }
            };

            return View(model);
        }
    }
}