﻿using System.Reflection;

// General Info
[assembly: AssemblyCompany("Roman V")]
[assembly: AssemblyProduct("Skill Matrix")]
[assembly: AssemblyCopyright("Copyright (C) 2019 Roman Vedmediuk")]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

// Version
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
